import React from 'react'
import { Platform, View, Text, TouchableOpacity, TouchableNativeFeedback, Button, TextInput} from "react-native";

const ViewItem = ({ item }) => {

  const handlePress = () => {
    console.log("on press");
  }

  const isAndroid = Platform.OS === "android"
  let TouchComponent = TouchableOpacity;
  if (isAndroid) {
    TouchComponent = TouchableNativeFeedback;
  }

  return (
    <TouchComponent onPress={handlePress} >
      <View style={{width:'100%', flexDirection:'row',alignItems:'center', justifyContent:'space-around', borderTopWidth:1, borderBottomWidth:1, padding:5, width:'100%'}}>
        <Text style={{fontSize:18}}>{item}</Text>
        <View style={{width:'20%'}}>
            <TextInput style={{textAlign:'center'}} value="1" />
            <Button title='X'></Button>
        </View>
      </View>
    </TouchComponent>
  )
}
export default ViewItem;
