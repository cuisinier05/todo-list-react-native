import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

const AddItem = ({ onAdd }) =>{
    const [pendingItem, setPendingItem] = useState("");

    const handlePress = () => {
        if (onAdd) {
        onAdd(pendingItem);
        }
    }
    return(
        <View style={stylesAddItem.footerContainer}>
            <TextInput id="textInput" value={pendingItem} onChangeText={setPendingItem} placeholder="Entrer un item" />
            <Button onPress={handlePress}  title='Ajouter'></Button>
        </View>
    )
}

const stylesAddItem = StyleSheet.create({
    footerContainer:{
        flex:0.2,
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around',
        borderTopWidth:1
    }
})

export default AddItem;