import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Header = () => {
    return(
        <View style={stylesHeader.headerContainer}>
            <Text style={stylesHeader.headerTextContainer}>MA LISTE</Text>
        </View>
    )
}

const stylesHeader = StyleSheet.create({
    headerContainer:{
        flex:0.3,
        justifyContent:'center',
        alignItems:'center',
        height:150,
        width:'100%',
        backgroundColor:'lightblue'
    },
    headerTextContainer:{
        fontSize:20
    }
})

export default Header;