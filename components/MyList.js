import React from 'react'
import { FlatList, StyleSheet } from 'react-native';
import ViewItem from './ViewItem';


const MyList = ({ data }) =>{
  return (
    <FlatList
      keyExtractor={(item, index) => `${item}_${index}`}
      data={data}
      renderItem={(itemData) => <ViewItem item={itemData.item} />}/>
  )
}

const stylesList = StyleSheet.create({
    listContainer:{
        flex:2.5,
        width:'100%',
        borderTopWidth:1,
        paddingTop:10
    },
    textListTitle:{
        fontSize:18
    }
})

export default MyList;