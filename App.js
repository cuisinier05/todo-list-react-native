import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './components/Header';
import MyList from './components/MyList';
import AddItem from './components/AddItem';

export default function App() {
  const [items, setItems] = useState([]);

  const handleAddItem = (text) => {
    setItems((currentItems) => [...currentItems, text]);
  }
  return (
    <View style={styles.container}>
      <Header />
      <MyList data={items} />
      <AddItem onAdd={handleAddItem} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
